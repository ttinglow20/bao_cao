#include<stdio.h>
#include<string.h>
#include<stdbool.h>
typedef struct
{
	char name[50];
	float ID;
	float salary;
}NhanVien;


void NhapThongTinNhanVien(NhanVien* NV)
{
	printf("Nhap ten nhan vien:"); fflush(stdin); gets(NV->name);
	printf("\nMa so nhan vien:"); scanf("%f", &(NV->ID));
	printf("\nLuong hang thang:"); scanf("%f", &(NV->salary));
	
}

void XuatThongTinNhanVien(int n, NhanVien A[])
{
	printf("\n%-15s %-15s %-15s","Ten Nhan Vien", "ID","Luong Hang Thang");
	int i=0;
	for(i = 0; i < n; i++)
	{
		printf("\n%-15s %-15.1f %-15.1f", A[i].name, A[i].ID, A[i].salary);
	}
}

void ThemNhanVien(int n, NhanVien A[])
{
	
	int i;
	for(i = n - 1; i > 0; i--)
	{
		printf("\nNhan Vien thu %d: \n", i+1);
		NhapThongTinNhanVien(&A[i]);
	}
	XuatThongTinNhanVien(n, A);
}

void XoaNhanVienTheoten(NhanVien A[], int *n, const char *xoa)
{
    int i;
    bool found = false;

    for (i = 0; i < *n; i++)
    {
        if (strcmp(A[i].name, xoa) == 0)
        {
            found = true;
            break;
        }
    }

    if (found)
    {
        int j;
        for (j = i; j < (*n - 1); j++)
        {
            A[j] = A[j + 1];
        }

        (*n)--;
        printf("Da xoa nhan vien co ten %s\n", xoa);
        XuatThongTinNhanVien(*n, A);
    }
    else
    {
        printf("Khong tim thay nhan vien co ten %s\n", xoa);
    }
}

void timkiemtheoten(int n, NhanVien A[])
{
	printf("Nhap ten nhan vien can tim: ");
	char name[50];
	fflush(stdin);
	gets(name);
	
	bool check = 0;
	int i;
	for(i = 0; i < n; i++)
	{
		if(stricmp(A[i].name, name) == 0){
			check = 1;
			printf("\n%-15s %-15.1f %-15.1f", A[i].name, A[i].ID, A[i].salary);
		}
	}
	if(check == 0)
	{
		printf("=> Khong tim thay nhan vien nao");
	}
}
void XoaNhanVienTheoID(NhanVien A[], int* n, float ID)
{
    int i;
    bool found = false;

    for (i = 0; i < *n; i++)
    {
        if (A[i].ID == ID)
        {
            found = true;
            break;
        }
    }

    if (found)
    {
        int j;
        for (j = i; j < (*n - 1); j++)
        {
            A[j] = A[j + 1];
        }

        (*n)--;
        printf("Da xoa nhan vien co ID %.1f\n", ID);
        XuatThongTinNhanVien(*n, A);
    }
    else
    {
        printf("Khong tim thay nhan vien co ID %.1f\n", ID);
    }
}

// H�m t�m kiem nh�n vi�n theo ID
void TimKiemTheoID(int n, NhanVien A[], float ID)
{
    bool found = false;
	int i;
    for (i = 0; i < n; i++)
    {
        if (A[i].ID == ID)
        {
            found = true;
            printf("\n%-15s %-15.1f %-15.1f", A[i].name, A[i].ID, A[i].salary);
        }
    }

    if (!found)
    {
        printf("Khong tim thay nhan vien co ID %.1f\n", ID);
    }
}
int main(){
	int n = 1 ;
	int chon = 1;
	NhanVien NV[100];
	
	printf("\n----CHUONG TRINH QUAN LY NHAN VIEN----\n");
	n++;
	NhapThongTinNhanVien(NV);
	
	while(chon !=0){
		printf("\n1.Them Nhan Vien\n");
		printf("2.Tim nhan vien theo ten");
		printf("\n3.Xoa nhan vien theo ten");
		printf("\n4.Xoa nhan vien theo ID");
		printf("\n5.Tim nhan vien theo ID");
		printf("\n0.Thoat");
		printf("\nChon: ");
		scanf("%d", &chon);
		
		switch(chon){
			case 1:
				printf("Them");
				ThemNhanVien(n, NV);
				break;
			case 2:
				printf("\ntim kiem\n");
				timkiemtheoten(n, NV);
				break;
			case 3:
			printf("\nXoa nhan vien ten\n");
            char xoa[50];
            printf("Nhap ten nhan vien can xoa: ");
            fflush(stdin);
            gets(xoa);
            XoaNhanVienTheoten(NV, &n, xoa);
            break;
			case 4:
                printf("\nXoa nhan vien theo ID\n");
                float id;
                printf("Nhap ID nhan vien can xoa: ");
                scanf("%f", &id);
                XoaNhanVienTheoID(NV, &n, id);
                break;
            case 5:
                printf("\nTim kiem nhan vien theo ID\n");
                printf("Nhap ID nhan vien can tim: ");
                scanf("%f", &id);
                TimKiemTheoID(n, NV, id);
                break;
			case 0:
				return 0;
			}
	}
}
