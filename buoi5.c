#include<stdio.h>
#include<ctype.h>
void separateCharacters(FILE* inputFile, FILE* digitFile, FILE* letterFile, FILE* specialCharFile)
{
    int ch;

    while ((ch = fgetc(inputFile)) != EOF) {
        if (isdigit(ch)) {
            fputc(ch, digitFile);
        } else if (isalpha(ch)) {
            fputc(ch, letterFile);
        } else {
            fputc(ch, specialCharFile);
        }
    }
}
int main()
{
	FILE* inputFile = fopen("input.txt","r");
	FILE* digitFile = fopen("digitFile.txt","w");
	FILE* letterFile = fopen("letterFile.txt","w");
	FILE* specialFile = fopen("specialFile.txt","w");
	if(inputFile == NULL || digitFile == NULL || letterFile == NULL || specialFile == NULL )
	{
		printf("khong mo duoc file\n");
		return 1;
	}
	separateCharacters(inputFile, digitFile, letterFile, specialFile);
	fclose(inputFile);
	fclose(digitFile);
	fclose(letterFile);
	fclose(specialFile);
	printf("t�ch file th�nh c�ng");
}
